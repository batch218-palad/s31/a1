// 1. What directive is used by Node.js in loading the modules it needs?
	// require


// 2. What Node.js module contains a method for server creation?
	// http module


// 3. What is the method of the http object responsible for creating a server using Node.js?
	// createServer()


// 4. What method of the response object allows us to set status codes and content types?
	//  writeHead()


// 5. Where will console.log() output its contents when run in Node.js?
	// git bash / terminal


// 6. What property of the request object contains the address's endpoint?
	//  url


//-------------------------



const http = require("http");

const port = 3000;

const server = http.createServer(function(request, response){

		if(request.url == "/login"){
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Welcome to the login page.");
		}

		else{ 
			response.writeHead(404, {"Content-Type" : "text/plain"});
			response.end("I'm sorry the page you are looking for cannot be found.");
		}

});



server.listen(port);
console.log(`The server is now accessible at localhost: ${port}`);








